//
//  main.cpp
//  CommandLineTool
//
//  Created by Tom Mitchell on 08/09/2017.
//  Copyright © 2017 Tom Mitchell. All rights reserved.
//

#include "Classes.hpp"

int main()
{
    LinkedList a;
    
    a.add(1);
    a.add(2);
    a.add(3);
    a.add(4);

    std::cout << "the value of that node is " << a.get(89)  << std::endl;
    
    a.display();
    
    a.remove(1);
    
    a.display();
    
    return 0;
}
