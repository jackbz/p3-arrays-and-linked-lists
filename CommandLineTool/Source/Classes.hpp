//
//  Classes.hpp
//  CommandLineTool
//
//  Created by Jack on 07/10/2017.
//  Copyright © 2017 Tom Mitchell. All rights reserved.
//

#ifndef Classes_hpp
#define Classes_hpp

#include <stdio.h>
#include <cmath>
#include <iostream>

////////////////////////////////////////////////////////////////////////////////////////////////////

class Array
{
public:
    /**Constructor*/
    Array();
    
    /**Destructor*/
    ~Array();
    
    /**Adds new items to the end of the array*/
    void add (float itemValue);
    
    /**Returns the item at the index*/
    float get (int index) const;
    
    /**Returns the number of items currently in the array*/
    int size();
    
    /**Removes an item from the array*/
    void remove(int index);
    
    /**Reverses order of elements in array*/
    void reverse();
    
private:
    int arraySize;
    float* currentArray;
};

////////////////////////////////////////////////////////////////////////////////////////////////////

class LinkedList
{
public:
    /**Constructor*/
    LinkedList();
    
    /**Destructor*/
    ~LinkedList();
    
    /**Adds new items to the end of the list*/
    void add (float itemValue);
    
    /**Returns the item stored in the node at the specified index*/
    float get (int index) const;
    
    /**Returns the number of items currently in the array*/
    int size();
    
    /**Displays every item in list in order */
    void display();
    
    /**Removes an item from the list*/
    void remove(int index);
    
    /**Reverses item order of list*/
    void reverse();
    
private:
    struct Node
    {
        float value;
        Node* next;
    };
    Node* head;
    Node* tail;
    int listSize;
};

////////////////////////////////////////////////////////////////////////////////////////////////////

#endif /* Classes_hpp */
