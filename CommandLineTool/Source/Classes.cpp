//
//  Classes.cpp
//  CommandLineTool
//
//  Created by Jack on 07/10/2017.
//  Copyright © 2017 Tom Mitchell. All rights reserved.
//

#include "Classes.hpp"

//////////////////////////////////////////////////////////////////////////////////////////////////

Array::Array(): arraySize (0), currentArray (nullptr)
{
    
}
Array::~Array()
{
    delete[] currentArray;
}

void Array::add (float itemValue)
{
    //allocates a new array whose size is size + 1
    float* tempArray = new float[arraySize+1];
        
    //copies contents of the current array to a temporary one
    for (int i = 0; i < arraySize; i++)
        {
            tempArray[i] = currentArray[i];
        }

    //this temp array has a variable size, so copy the new value to the end of this one
    tempArray[arraySize] = itemValue;
    
    //delete all the date in the current array
    delete[] currentArray;
    
    //copy the data from the dynamic, temporary array into the actual array we use
    currentArray = tempArray;
    
    //incremement the array size by 1
    arraySize = arraySize + 1;
    
}

float Array::get (int index) const
{
    return currentArray[index];
}

int Array::size()
{
    return arraySize;
}

void Array::remove(int index)
{
    //allocates a new array whose size is size - 1
    float* tempArray = new float[arraySize-1];
    
    //copies contents of the current array to a temporary one up until index that needs deleting
    for (int i = 0; i < index; i++)
    {
        tempArray[i] = currentArray[i];
    }
    
    //continue copying from current array, skipping the index that needs deleting
    for (int i = index; i < arraySize; i++)
    {
        tempArray[i] = currentArray[i+1];
    }

    //delete all the date in the current array
    delete[] currentArray;
    
    //copy the data from the dynamic, temporary array into the actual array we use
    currentArray = tempArray;
    
    //decrement the array size by 1
    arraySize = arraySize - 1;

}

void Array::reverse()
{
    //allocates a new array whose size is the current size
    float* tempArray = new float[arraySize];
    
    //copies contents of the current array to a temporary one, though starts at the end of the current array
    for (int i = 0; i < arraySize; i++)
    {
        tempArray[i] = currentArray[arraySize - 1-i];
    }
    
    //delete all the date in the current array
    delete[] currentArray;
    
    //copy the data from the dynamic, temporary array into the actual array we use
    currentArray = tempArray;

}

//////////////////////////////////////////////////////////////////////////////////////////////////

LinkedList::LinkedList()
{
    head = nullptr;
    tail = nullptr;
    listSize = 0;
}
LinkedList::~LinkedList()
{
    delete[] head;
    delete[] tail;
}
void LinkedList::add (float itemValue)
{
    Node* tempNode = new Node;
    tempNode->value = itemValue;
    tempNode->next = nullptr;
    
    if(head == nullptr)
    {
        head = tempNode;
        tail = tempNode;
    }
    else
    {
        tail->next = tempNode;
        tail = tail->next;
    }
    
}
float LinkedList::get (int index) const
{
    if (index >= listSize)
    {
        std::cout << "\nERROR: NODE DOESN'T EXIST\n";
        return 0;
    }
    else
    {
    Node* tempNode;
    tempNode = head;
    int count;
    while (count != index+1)
    {
        count++;
        tempNode = tempNode->next;
    }
    return tempNode->value;
    }
}
int LinkedList::size()
{
    Node* tempNode;
    tempNode = head;
    int index = 0;
    while (tempNode != nullptr)
    {
        index++;
        tempNode = tempNode->next;
    }
    listSize = index;
    return listSize;
    
}
void LinkedList::display()
{
    Node* tempNode;
    tempNode = head;
    while (tempNode != nullptr)
    {
        std::cout << tempNode->value << std::endl;
        tempNode = tempNode->next;
    }
}

void LinkedList::remove(int index)
{
    int count;
    
    //cycles through list until before the node that needs deleting
    while (count != index-1)
    {
        count++;
        tail = tail->next;
    }
    
    //skips over the node that is gonna be deleted
    tail = tail->next->next;
   
    
    //continues until it gets to nullptr or someething who knows
    while (tail != nullptr)
    {
        tail = tail->next;
    }
    
}

void LinkedList::reverse()
{
    
}

//////////////////////////////////////////////////////////////////////////////////////////////////




